# JS Helpers, by Billow

A useful set of JS helpers for Billow's Vue apps. Includes a component mixin, a bunch of filters, and string utilities.

## Installation

```shell
npm install @billow/js-helpers
```

## Query String mixin

Used for managing tabs through window history, the query-string mixin is obtainable from `helpers.mixins.queryString`. In your component:

```js
import { mixins } from '@billow/js-helpers'

export default {
  mixins: [
    mixins.queryString,
  ],
}
```

To set a tab, call `this.setTab(tabName)`.  The active tab can be obtained from `this.activeTab`.

> Note that this mixin relies on a Vuex store being injected, and interacts with a namespaced module called `app`. This module must have a mutator named `setTab` which accepts the name of a tab and mutates it in state, as well as a getter named `tab`, which simply returns the name of the tab from wherever it lives in state.

If you'd prefer, you can also import the mixin directly:

```js
import queryStringMixin from '@billow/js-helpers/src/mixins/query-string'
```

## FormData utility

If you need to leverage the FormData API to submit multipart form data via an XHR request, you can make use of this utility, which merges an object's keys and values (often from state) together, whilst retaining integrity of data that would often be lost without passing through this API, such as a File.

```js
import { utilities } from '@billow/js-helpers'

// Somewhere in a store module, like an action:
let payload = utilities.formData({
  ...state.userProfile,
  picture: state.profilePicture
})

// Or if your file (a picture, in this example) is in the same state key:
let payload = utilities.formData(state.userProfile)
```

If you'd prefer, you can also import the utility directly:

```js
import makeFormData from '@billow/js-helpers/src/utilities/form-data'
```

## String utility

A simple utility that currently exports a `format` helper, which assists in formatting a string using named keys. A filter is also available to leverage this utility.

```js
import { utilities } from '@billow/js-helpers'

utilities.string.format('{key} equals :value', {
  key: this.generateKey(),
  value: this.value,
})
```

If you'd prefer, you can also import the utility directly:

```js
import { format } from '@billow/js-helpers/src/utilities/string'
```

## Filters

```js
import { filters } from '@billow/js-helpers'

// Install the filters
Vue.use(filters)
```

The available filters are listed below in alphabetical order with a method signature (if available) and at least one example.

You can access filters using `{{ var | filter([args]) }}` in your component template, or `this.$options.filters.filter(var[, args])` in your JS.

> 💡 Some filter-arguments are swappable, meaning that `{{ a | b(c) }}` and `{{ c | b(a) }}` would produce the same result.

### asAccounting

Arguments: `(currency = 'R', precision = 2, thousandsSeparator = ',', precisionSeparator = '.', format = '%s %v')`

Examples:

```js
2000 | asAccounting // R 2,000.00
2000 | asAccounting('$') // $ 2,000.00
15982.115 | asAccounting('R', 3) // R 15,982.115
2000 | asAccounting('R', 2, ' ') // R 2 000.00
2000 | asAccounting('€', 2, '.', ',') // € 2.000,00
2000 | asAccounting('CAD', 2, ',', '.', '%v %s') // 2,000.00 CAD
```

### asCurrency

Signature: `(currency = 'R ', locale = 'en-GB')`

Examples:

```js
2000 | asCurrency // R 2,000.00
2000 | asCurrency('$') // $ 2,000.00
123456.789 | asCurrency('₹', 'en-IN') // ₹ 1,23,456.789
```

### asMoney

Signature: `(fractionDigits = 2, maximumFractionDigits = 2, locale = 'en-GB')`

Examples:

```js
2000 | asMoney // 2,000.00
2000.211 | asMoney(3) // 2,000.211
2000.21109 | asMoney(3, 5) // 2,000.21109
12345678.7891 | asMoney(3, 5, 'en-IN') // 1,23,456.7891
```

### asPlain

Convert HTML to plain text.

```html
<template>
  <div>{{ body | asPlain }}</div>
</template>
<!-- In laoreet exercitation? Accusantium, pede? Nascetur, [...] -->
```

```js
export default {
  data: () => ({
    body: '<p>In laoreet exercitation? Accusantium, pede? <br><br> Nascetur, aspernatur pretium feugiat, nullam rerum suscipit pulvinar dignissimos nulla sint, rerum dolorum fames hac.</p>'
  })
}
```

### date

Signature: `(format = 'Y-MM-DD')`

Examples

```js
'2018-05-24 12:00:00' | date // 2018-05-24
'2018-05-24 17:00:00' | date('ddd, hA') // Thur, 5PM
```

### filesize

Signature: `(base = 2, exponent = -1)`

Example:

```js
38889823 | filesize // 37.09 MB
38889823 | filesize(2, 3) // 0.04 GB
```

### format

Format a string using named keys. Arguments are swappable.

Signature: `(format|object)`, where the argument is the format string and the input is the object from which data is obtained, or vice-versa.

Examples:

```js
user | format(':name has role of :role') // Mike has a role of member
':name has role of :role' | format(user) // Mike has a role of member
```

```js
user = {
  name: 'Mike',
  role: 'member'
}
```

### fromNow

Signature: `(suffix = true)`

Examples:

```js
'2018-09-10' | fromNow // in 4 months
'2018-05-24 07:24:00' | fromNow // 4 minutes ago
'2018-05-24 07:24:00' | fromNow(false) // 4 minutes
```

### log

Logs the input to the console.

Example:

```js
'I’m in the console' | log
```

### nl2br

Convert line breaks to HTML breaks

Example:

```html
<div v-html="$options.filters.nl2br(multilineString)"></div>
```

### or

Similar to using the logical-or (`a || b`, where `a` is falsy for example), only `or` uses a stricter truthy-check, unless `loose` is true.

Signature: `(fallback, loose = false)`

Examples:

```js
undefined | or('n/a')
null | or('n/a')
'' | or('n/a')

0 | or('n/a') // 0
0 | or('n/a', true) // n/a
false | or('n/a') // false
false | or('n/a', true) // n/a
```

### ordinal

Example:

```js
2 | ordinal // 2nd
4 | ordinal // 4th
```

### plural

Pluralise a word when the input is a number greater than 1. Arguments are swappable.

Signature: `(count|singular)`, where the argument is the singular word and the input is the count, or vice-versa.

Examples:

```js
1 | plural('apple') // apple
2 | plural('apple') // apples
1 | plural('fish') // fish
2 | plural('fish') // fish

'apple' | plural(1)
'apple' | plural(2)
'fish' | plural(1)
'fish' | plural(2)
```

### replace

Signature: `(search, replace)`

Examples:

```js
'foo bar' | replace('foo', 'boo') // boo bar
'A123E' | replace(/\d/, '*') // A*23E
```

### toFixed

Signature: `(precision = 2)`

Example:

```js
"2.983726" | toFixed // 2.98
```

### truncate

Signature: `(length = 20, omission = '…', separator = /,? +/)`

Examples:

```js
'Error gravida dis phasellus deleniti, nostrud.' | truncate
// Error gravida dis…

'Error gravida dis phasellus deleniti, nostrud.' | truncate('[…]')
// Error gravida dis[…]
```

### ucfirst

Example:

```js
'billow' | ucfirst // Billow
```

### ucwords

Example:

```js
'do you use vue?' | ucwords // Do You Use Vue?
```
