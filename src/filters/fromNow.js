import moment from 'moment'

// {{ date | fromNow([true|false])}}
export default (date, suffix = true) => {
  return moment(date).fromNow(!suffix)
}
