// {{ amount | asMoney([4[, 2, [ 'en-US']]) }}
export default (amount = 0, fractionDigits = 2, maximumFractionDigits = 2, locale = 'en-GB') => {
  return amount.toLocaleString(locale, {
    style: 'decimal',
    minimumFractionDigits: fractionDigits,
    maximumFractionDigits: maximumFractionDigits
  })
}
