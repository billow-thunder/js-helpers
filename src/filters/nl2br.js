// <div v-html="$options.filters.nl2br(multilineString)"></div>
export default input => {
  return String(input).replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, `$1<br>$2`)
}
