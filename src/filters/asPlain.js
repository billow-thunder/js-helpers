import he from 'he'

// {{ html | asPlain }}
export default (html = null) => {
  return Boolean(html)
    ? he.decode(html).replace(/<\/?[^>]+>/ig, ' ').trim()
    : ''
}
