// {{ value | or('n/a') }}
export default (input, fallback, loose = false) => {
  return (input === undefined || input === '' || input === null || isNaN(input) || (loose && !Boolean(input))) ? fallback : input;
}
