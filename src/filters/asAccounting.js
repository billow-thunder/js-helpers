import { formatMoney } from 'accounting-big'

// {{ amount | asAccounting('$') }}
export default (amount, currency = 'R', precision = 2, thousandsSeparator = ',', precisionSeparator = '.', format = '%s %v') => {
  return formatMoney(amount, currency, precision, thousandsSeparator, precisionSeparator, format)
}
