import { truncate } from 'lodash'

// {{ string | truncate(50[, '………', '.']) }}
export default (string, length = 20, omission = '…', separator = /,? +/) => {
  return truncate(string, { length, omission, separator });
}
