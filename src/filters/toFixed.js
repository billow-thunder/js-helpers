// {{ string | toFixed }}
export default (input, precision = 2) => {
  return Number(input).toFixed(precision)
}
