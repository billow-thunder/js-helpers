// {{ string | replace('foo', 'baz')}}
export default (string, search, replacement) => {
  return string.replace(search, replacement)
}
