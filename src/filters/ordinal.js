import inflector from 'lodash-inflection'

// {{ number | ordinal }}
export default value => {
  return inflector.ordinalize(value)
}
