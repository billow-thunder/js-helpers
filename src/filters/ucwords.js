import { titleize } from 'lodash'

// {{ string | ucwords }}
export default string => {
  return titleize(string);
}
