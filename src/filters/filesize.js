import filesize from 'filesize'

// {{ size | filesize }}
// {{ size | filesize(2, 3) }}
export default (value, base = 2, exponent = -1) => {
  return filesize(value, { base, exponent })
}
