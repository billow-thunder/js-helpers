import moment from 'moment'

// {{ date | date(['Y-m-d'])}}
export default (date, format = 'Y-MM-DD') => {
  return moment(date).locale(navigator.language || 'en-US').format(format)
}
