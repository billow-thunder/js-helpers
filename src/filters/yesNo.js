// {{ condition | yesNo }}
// {{ condition | yesNo('Active', 'Inactive') }}
export default (condition, yes = 'Yes', no = 'No') => {
  return Boolean(condition) ? yes : no;
}
