import moment from 'moment'

// {{ someDate | asCalendar }}
export default (date, format = 'Y-MM-DD') => {
  return moment(date).calendar(null, {
    sameElse: format
  })
}
