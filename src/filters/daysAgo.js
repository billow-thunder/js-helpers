import moment from 'moment'

// {{ date | daysAgo()}}
export default (date) => {
  return moment().diff(moment(date), 'days')
}
