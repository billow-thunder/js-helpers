// {{ amount | asCurrency(['$']) }}
export default (amount = 0, currency = 'R', locale = 'en-GB') => {
  return currency.trim() + ' ' + amount.toLocaleString(locale, {
    style: 'decimal',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  })
}
