import inflector from 'lodash-inflection'

// {{ count | plural('item') }}
// or {{ 'item' | plural(count) }}
export default (left, right) => {
  let [singular, count] = typeof left === 'string' ? [left, right] : [right, left]
  return inflector.pluralize(singular, count)
}
