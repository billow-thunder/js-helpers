let queryString = require('querystring')

export default query => {

  let _qs = queryString.stringify(_(query).omitBy(_.isEmpty).value())  

  return (_qs.length > 0) ? '?' + _qs : ''

}